using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropItem : MonoBehaviour , IDropHandler
{
    public ItemProperties itemProper;
    public bool canDropItem, empty;
    public Slot slotparent;
    public void OnDrop(PointerEventData eventData)
    {
        GameObject dropped = eventData.pointerDrag;
        DraggableItem draggable = dropped.GetComponent<DraggableItem>();
        if (draggable.originalParent == transform || (canDropItem && empty))
        {
            slotparent = draggable.slot;
            if (!slotparent.empty)
            {
                draggable.parentAfterDrag = transform;
                itemProper = draggable.itemProper;
                empty = false;
            }
        }
    }

}
