using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.Experimental.Rendering.Universal;

public class LuzIntermitente : MonoBehaviour
{
    public Light2D luz;
    public TipoLuz tipoLuz;
    [Header("Intermitente")]
    [Range(0,1)]public float rangoIntermitencia;

    [Header("Fade")]
    public AnimationCurve cosAnimationCurve, fadeAnimationCurve, animationCurve;
    public float velocidadFade, outerRadius, tiempoEsperaEncendido;
    bool fade;
    float valorXAnimationCurve, tiempo;
    int signo;
    void Start()
    {
        luz = GetComponent<Light2D>();
        tiempo = Random.Range(tiempoEsperaEncendido, tiempoEsperaEncendido + 2);
        signo = -1;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (tipoLuz==TipoLuz.fade)
        {
            if (animationCurve != cosAnimationCurve)
            {
                signo = 1;
                animationCurve = fadeAnimationCurve;
                fade = true;
            }
        }
    }

    void Update()
    {
        switch (tipoLuz)    
        {
            case TipoLuz.intermitente:
                float numRandom1 = Random.Range(0f,10f);
                if (numRandom1>5)
                {
                    float numRandom2 = Random.Range(1-rangoIntermitencia, 1+rangoIntermitencia);
                    luz.intensity = numRandom2;

                }
                break;
            case TipoLuz.fade:
                tiempo = tiempo + signo*Time.deltaTime;
                if (tiempo<0)
                {
                    animationCurve = cosAnimationCurve;
                    tiempo = 0;
                    signo = 1;
                    fade = true;
                }
                if (fade)
                {
                    valorXAnimationCurve += Time.deltaTime*velocidadFade;
                    luz.pointLightOuterRadius = animationCurve.Evaluate(valorXAnimationCurve)*outerRadius;
                    if (animationCurve.Evaluate(valorXAnimationCurve)==animationCurve.keys[animationCurve.length-1].value)
                    {
                        animationCurve = new AnimationCurve();
                        valorXAnimationCurve = 0;
                        tiempo = Random.Range(tiempoEsperaEncendido,tiempoEsperaEncendido+2);
                        signo = -1;
                        fade = false;
                    }
                }
                break;
            default:
                break;
        }
    }

    public enum TipoLuz
    {
        intermitente, fade
    }
}
