using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pociones : IUsoItems
{
    public TipoPocion tipoPocion;
    public void UsarItems()
    {
        switch (tipoPocion)
        {
            case TipoPocion.Salud:
                GameObject player = GameObject.FindGameObjectWithTag("Player");
                player.GetComponent<Life_bar>().TakeDamage(-20);
                break;
            case TipoPocion.Ataque:
                break;
            case TipoPocion.Velocidad:
                break;
            default:
                break;
        }
    }
}
