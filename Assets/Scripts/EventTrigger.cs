using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSEventos;
public class EventTrigger : MonoBehaviour
{
    public EventNames eventName;
    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            print("Espacio presionado");
            EventManager.TriggerEvent(EventData.Create(eventName)
                .Set("Position", new Vector3(1,4,8)));
           
        }
    }
}
