using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sounds : MonoBehaviour
{

    public static Sounds sound;
    public AudioClip[] sfx;
    public AudioSource asource;

    public void Awake()
    {
        if (sound == null)
        {
            sound = this;
        }

        else
        {
            Destroy(this);
        }
    }

    public void Active_SoundsFx(Vector3 soundPoints, int id)
    {
        transform.position = soundPoints;
        asource.PlayOneShot(sfx[id]);
    }
}
