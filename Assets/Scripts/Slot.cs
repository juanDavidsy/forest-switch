using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Slot : MonoBehaviour
{
    public Inventory inventario;
    //public GameObject item;
    public IUsoItems usoItems;

    public bool empty;

    public Transform slotIconGameObject;
    public Image itemDraggable;
    public int numberOfObjects;

    public TextMeshProUGUI numberObjText;


    public ItemProperties slotProperties;
    public ItemProperties slotVacio;

    public void Comenzar(Inventory inventory)
    {
        inventario = inventory;
        if (slotProperties == null)
        {
            empty = true;
            slotProperties = slotVacio;
        }
        else
            empty = false;
        UpdateSlot();
        UpdateNumberObj();
    }
    public void UpdateSlot()
    {
        slotIconGameObject.GetComponent<Image>().sprite = slotProperties.icon;
    }

    public void TurnOffIcon(bool estado)
    {
        slotIconGameObject.GetComponent<Image>().enabled = estado;
    }
    public void UpdateNumberObj()
    {
        if (numberOfObjects > 0)
        {
            numberObjText.text = numberOfObjects.ToString();
        }
        else
        {
            itemDraggable.sprite = slotVacio.icon;
            numberObjText.text = "";
        }
    }

    public void UseItem()
    {
        switch (slotProperties.type)
        {
            case ItemProperties.Tipo.item:
                break;
            case ItemProperties.Tipo.posiones:
                if (!empty)
                {
                    Pociones pocion = new Pociones{tipoPocion = slotProperties.tipoPocion};
                    usoItems = pocion;
                }
                break;
            default:
                break;
        }
        if (!empty)
        {
            usoItems.UsarItems();
            inventario.RemoveItemDirectly(this, 1);
        }
    }

}
