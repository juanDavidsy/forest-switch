using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSEventos;

public class Rotar : MonoBehaviour
{
    public EventNames eventName;
    public float velocity;
    bool empezar;

    private void OnEnable()
    {
        EventManager.StartListening(eventName, EmpezarRotacion);
    }

    void Update()
    {
        if (empezar)
            transform.Rotate(Vector3.forward*velocity);
    }

    public void EmpezarRotacion(EventData data)
    {
        print(data.Get<Vector3>("Position"));
        empezar = true;
    }

    private void OnDisable()
    {
        EventManager.StopListening(eventName, EmpezarRotacion);
    }
}
