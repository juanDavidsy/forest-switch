using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PortalTrigger : MonoBehaviour
{
    public NombresMapas nombreMapaDestino;
    public DatosPortal datosPortal;
    public Transform padrePortal;
    public bool sameMap;
    public float essenceNecesaryAmount;

    [Header("SameMap")]
    public Transform portalDestination;
 
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            float cantidadEscenciaBlanca = collision.GetComponent<EscenciaBlanca>().currentEssence;
            if (Input.GetKey(KeyCode.E) && cantidadEscenciaBlanca>=essenceNecesaryAmount)
            {
                
                collision.GetComponent<EscenciaBlanca>().CambiarCantidadEscencia(-essenceNecesaryAmount);
                if (!sameMap)
                {
                    datosPortal.onTp = true;
                    datosPortal.nombrePortal = padrePortal.name;
                    datosPortal.nombreMapaActual = SceneManager.GetActiveScene().name;
                    GetComponent<ChangeMap>().CambioEscena(nombreMapaDestino);
                }
                else
                {
                    collision.GetComponent<ToDestination>().Transportar(portalDestination.position);
                }
            }
        }
    }
}
