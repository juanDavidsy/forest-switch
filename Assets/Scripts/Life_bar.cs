using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Events;

public class Life_bar : MonoBehaviour
{
    public Image life_Bar;
    public Animator animator;
    public GameObject postDead, luz;

    public UnityEvent deadEvent;

    public float currentLife;
    public float maxLife, tiempoParaRevivir;

    bool onDie;

    private void Start()
    {
        
        currentLife = maxLife;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (life_Bar != null)
        {
            life_Bar.fillAmount = currentLife / maxLife;
        }
    }

    public void TakeDamage(float numero)
    {
        
        currentLife -= numero;
        if (transform.CompareTag("Player"))
        {
            if (currentLife>100)
            {
                currentLife = 100;
            }

        }
        if (currentLife > 0)
        {
            animator.SetTrigger("Hurt");
        }
        else
        {
            if (!onDie)
            {
                onDie = true;
                animator.SetTrigger("Dead");
                deadEvent.Invoke();
                
            }

        }

    }

    public void Muerte()
    {
        EstoyVivo(false);

    }

    public void PostDeadItem()
    {

        postDead.transform.position = transform.position;
        postDead.SetActive(true);
    }
    public void EstoyVivo(bool vivo)
    {
       
        if (postDead!=null)
        {
            postDead.transform.position = transform.position;
            postDead.SetActive(!vivo);
        }
        GetComponent<Rigidbody2D>().simulated = vivo;
        EnemyBehaviorTopDown behavior;
        if (TryGetComponent<EnemyBehaviorTopDown>(out behavior))
        {
            GetComponent<MoveEnemyTopDown>().enabled = vivo;
            BoxCollider2D boxCollider;
            if (TryGetComponent<BoxCollider2D>(out boxCollider))
            {
                GetComponent<BoxCollider2D>().enabled = vivo;
            }
            else
            {
                GetComponent<CapsuleCollider2D>().enabled = vivo;
            }
            behavior.enabled = vivo;
            GetComponent<SpriteRenderer>().enabled = vivo;
        }
        if (transform.CompareTag("Player"))
        {
            Move move;
            if (TryGetComponent<Move>(out move))
            {
                move.enabled = vivo;
                GetComponent<Jump>().enabled = vivo;
                GetComponent<CapsuleCollider2D>().enabled = vivo;
            }
            else
            {
                GetComponent<MovePlayerTopDown>().enabled = vivo;
                GetComponent<BoxCollider2D>().enabled = vivo;
            }
            
        }
        if (!vivo)
        { 
            StartCoroutine(CTiempoParaRevivir());
        }
        else
        {
            if (luz!=null)
            {
                luz.GetComponent<Light2D>().pointLightOuterRadius = 3;
            }
            currentLife = maxLife;
        }
        onDie = false;
    }

    public void preDead()
    {
        GetComponent<EnemyBehaviorTopDown>().enabled = false;
        GetComponent<MoveEnemyTopDown>().onMove = false;
        if (luz != null)
        {
            luz.GetComponent<Light2D>().pointLightOuterRadius = 7;
        }
    }
    IEnumerator CTiempoParaRevivir()
    {
        yield return new WaitForSeconds(tiempoParaRevivir);
        if (!transform.CompareTag("Player"))
        {
            EstoyVivo(true);
        }

    }


}
