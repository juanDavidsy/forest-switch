using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using FSEventos;
public class DIalogoNPC : MonoBehaviour
{
    
    public TextMeshProUGUI texto;
    //public AudioSource fuenteAudioTexto;
    [SerializeField] float tiempoDuracionTexto;
    //public GameObject image;

    public EventNames eventName;
    //private const string EVENT_DIALOGO = "Dialogo";

    //public bool tutorial;
    //public GameObject[] muros;
    void Start()
    {
        //GameManager.Instance.OnDialogo += ActivarDialogo; // Se subscribe al evento OnDialogo
        //GameManager.Instance.DialogoDuranteJuego(this, dialogoIntro.dialogo); //Llama a la funcion que invoca el evento OnDialogo
    }
    private void OnEnable()
    {
        EventManager.StartListening(eventName, ActivarDialogo);
    }
    private void OnDisable()
    {
        EventManager.StopListening(eventName, ActivarDialogo);
       
    }
    //Funcion que esta subscrita al evento OnDialogo 
    //public void ActivarDialogo(object sender, Dialogos.Texto e) 
    //{
    //    StartCoroutine(ReproducirDialogo(e));
    //}
    public void ActivarDialogo(EventData data)
    {
        StartCoroutine(ReproducirDialogo(data.Get<Dialogos.Texto>("Texto")));
    }
    //corrutina que reproduce los dialogos
    IEnumerator ReproducirDialogo(Dialogos.Texto lista)
    {
        //GameManager.Instance.enDialogo = true;
        //image.SetActive(true);
        yield return new WaitForSeconds(1);
        foreach (var item in lista.contenedor)
        {
            texto.text = item.texto;
            //fuenteAudioTexto.clip = item.audioTexto;
            //tiempoDuracionTexto = item.audioTexto.length;
            yield return new WaitForSeconds(tiempoDuracionTexto);
        }
        texto.text = "";
        //image.SetActive(false);
        //GameManager.Instance.enDialogo = false;
    } 
}
