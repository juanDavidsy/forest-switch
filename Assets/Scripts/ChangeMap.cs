using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ChangeMap : MonoBehaviour
{
    internal void CambioEscena(NombresMapas nombre)
    {
        string textoNombre = nombre.ToString();
        SceneManager.LoadScene(textoNombre);
    }
}
