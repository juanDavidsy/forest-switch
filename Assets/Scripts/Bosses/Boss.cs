using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    public Animator animator;

    public Rigidbody2D rb;

    public Transform player;

    public Transform spawnPoint;

    public bool rigth;

    bool muertini;

    public AudioSource music;

    [Header("Vida")]
    [SerializeField]private Life_bar life;

    [Header("Ataque")]
    [SerializeField] private Transform attackPoint;
    [SerializeField] private float attackRadius;
    [SerializeField] private float attackDamage;

    
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        life = GetComponent<Life_bar>();
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector2.Distance(transform.position, player.position);
        animator.SetFloat("Distance", distance);
        if (!muertini)
        {
            GetDamage();
        }
    }

    public void GetDamage()
    {

        if(life.currentLife <= 0)
        {
            animator.SetTrigger("Death");
            muertini = true;
            GetComponent<CapsuleCollider2D>().enabled = false;
            GetComponent<Rigidbody2D>().gravityScale = 0;
            music.Stop();
        }
    }
   
    public void LookPlayer()
    {
        if((player.position.x > transform.position.x && !rigth) || (player.position.x < transform.position.x && rigth))
        {
            rigth = !rigth;
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);
        }
    }
    
    public void Attack()
    {
        Collider2D[] objetos = Physics2D.OverlapCircleAll(attackPoint.position, attackRadius);

        foreach(Collider2D collision in objetos)
        {
            if (collision.CompareTag("Player"))
            {
                collision.GetComponent<Life_bar>().TakeDamage(attackDamage);
            }
        }
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPoint.position, attackRadius);
    }
}
