using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Spell : MonoBehaviour
{
    [SerializeField] private float damage;
    [SerializeField] private Vector2 boxDimension;
    [SerializeField] private Transform boxPosicion;
    [SerializeField] private float lifeTime;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, lifeTime);
    }

    public void Strike()
    {
        Collider2D[] objetos = Physics2D.OverlapBoxAll(boxPosicion.position, boxDimension, 0f);

        foreach (Collider2D colision in objetos)
        {
            if(colision.CompareTag("Player"))
            {
                colision.GetComponent<Life_bar>().TakeDamage(damage);
            }
        }
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(boxPosicion.position, boxDimension);
    }
}
