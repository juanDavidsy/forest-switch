using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerBoss : MonoBehaviour
{
    [SerializeField] private GameObject boss;
    public UnityEvent enterBossEvent;
    public SonidosAmbiente sonidosAmbiente;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            boss.SetActive(true);
            sonidosAmbiente.enviroment.Stop();
            if (boss.GetComponent<Life_bar>().currentLife>0)
            {
                enterBossEvent.Invoke();
            }
        }
    }

}
