using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Executioner_summon : MonoBehaviour
{
    [SerializeField] private float damage;
    [SerializeField] private Transform attackPoint;
    [SerializeField] private float attackRadius;
    [SerializeField] private float attackDamage;
    [SerializeField] private float lifeTime;

    public Transform player;
    public float velocidadBolita;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        transform.rotation = Quaternion.Euler(0, 0, 90);
        if (transform.position.x>player.transform.position.x)
        {
            GetComponent<SpriteRenderer>().flipY = true;
        }
        
        GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(player.transform.position.x - transform.position.x)*velocidadBolita,0);
        Destroy(gameObject, lifeTime);
    }

    public void Strike()
    {
        Collider2D[] objetos = Physics2D.OverlapCircleAll(attackPoint.position, attackRadius);

        foreach (Collider2D colision in objetos)
        {
            if (colision.CompareTag("Player"))
            {
                colision.GetComponent<Life_bar>().TakeDamage(damage);
            }
        }
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPoint.position, attackRadius);
    }

}
