using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscenciaBlanca : MonoBehaviour
{
    public Material materialEscenciaBlanca;
    internal float currentEssence;
    private void Start()
    {
        currentEssence = materialEscenciaBlanca.GetFloat("_Cantidad")*100/0.7f;
    }
    public void CambiarCantidadEscencia(float cantidad)
    {
        
        cantidad = materialEscenciaBlanca.GetFloat("_Cantidad")+(cantidad * 0.7f / 100f) ;
        if (cantidad>0.7f)
        {
            cantidad = 0.7f;
        }
        currentEssence = (cantidad * 100f / 0.7f);
        materialEscenciaBlanca.SetFloat("_Cantidad",cantidad);
    }
}
