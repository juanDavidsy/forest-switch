using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ascensor : MonoBehaviour
{
    //public bool manual, puertaAbierta;
    public float velocity;
    public AnimationCurve curvaMovimiento;
    public GameObject plataform;
    public Transform closedPosition;
    public Transform openPosition;
    public bool movimientoContinuo;
    
    bool andando, cerrar;

    Vector3 posicionInicial;
    float contador;
    private void OnEnable()
    {
        if (movimientoContinuo)
        {
            MovePlataform(false);
        }
    }
    private void Update()
    {
        if (andando)
        {
            contador += Time.deltaTime*velocity;
            if (!cerrar)
            {
                plataform.transform.position = Vector3.Lerp(posicionInicial, openPosition.position, curvaMovimiento.Evaluate(contador));
            }
            if (cerrar)
            {
                plataform.transform.position = Vector3.Lerp(posicionInicial, closedPosition.position, curvaMovimiento.Evaluate(contador));
            }
            if (curvaMovimiento.Evaluate(contador) == curvaMovimiento.keys[curvaMovimiento.length - 1].value)
            { 
                contador = 0;
                if (!movimientoContinuo)
                {
                    andando = false;
                }
                else
                {
                    MovePlataform(!cerrar);       
                }
            }
        }
    }
    //si es true la puerta se cierra
    public void MovePlataform(bool closed)
    {
        posicionInicial = plataform.transform.position;
        andando = true;
        cerrar = closed;
    }
}
