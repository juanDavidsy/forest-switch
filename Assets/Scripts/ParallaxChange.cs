using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxChange : MonoBehaviour
{
    public GameObject parallaxCueva, parallaxBosque;

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (collision.transform.position.y > transform.position.y)
            {
                parallaxBosque.SetActive(true);
                parallaxCueva.SetActive(false);
            }
            else
            {
                parallaxBosque.SetActive(false);
                parallaxCueva.SetActive(true);
            }
        }

    }
}
