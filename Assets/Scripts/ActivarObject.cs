using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarObject : MonoBehaviour
{
    public void Activar(GameObject objeto)
    {
        objeto.SetActive(true);
    }
    public void DesActivar(GameObject objeto)
    {
        objeto.SetActive(false);
    }
}
