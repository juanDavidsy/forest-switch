using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetearComponentes : MonoBehaviour
{
    public Material materialEscenciaBlanca;
    public DatosPortal datosPortal;

    public void Resetear()
    {
        materialEscenciaBlanca.SetFloat("_Cantidad",0);
        datosPortal.onTp = false;
    }
}
