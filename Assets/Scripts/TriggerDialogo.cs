using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSEventos;
public class TriggerDialogo : MonoBehaviour
{
    public Dialogos[] triggerDialogos;
    int contadorDeEntradas;

    public EventNames eventName;
    void Start()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "Player")// && !GameManager.Instance.enDialogo)
        {
            //GameManager.Instance.DialogoDuranteJuego(this,triggerDialogos[contadorDeEntradas].dialogo);
            EventManager.TriggerEvent(EventData.Create(eventName).Set("Texto",triggerDialogos[contadorDeEntradas].dialogo));
            if (contadorDeEntradas<triggerDialogos.Length-1)
                contadorDeEntradas += 1;
        }
    }
}
