using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDrop : MonoBehaviour
{
    public float dropEscenciaBlanca;
    public ItemProperties[] items;
    public SpriteRenderer spriteItem;
    int numItem;

    private void OnEnable()
    {
        numItem = Random.Range(0,items.Length);
        spriteItem.sprite = items[numItem].icon;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {

            if (items.Length>0)
            {
                collision.GetComponent<Inventory>().AddItem(items[numItem]);
            }
            collision.GetComponent<EscenciaBlanca>().CambiarCantidadEscencia(dropEscenciaBlanca);
            gameObject.SetActive(false);

        }
    }
}
