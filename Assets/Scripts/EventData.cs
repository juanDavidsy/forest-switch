using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FSEventos
{
    public class EventData 
    {
    
        public EventNames name;
        private Dictionary<string, object> data;

        public void Init(EventNames name)
        {
            data = new Dictionary<string, object>();
            this.name = name;
        }

        public static EventData Create(EventNames name)
        {
            var data = new EventData();
            data.Init(name);

            return data;
        }

        public EventData Set<T>(string key, T value)
        {
            if (!data.ContainsKey(key))
            {
                data.Add(key, value);
            }
            else
            {
                data[key] = value;
            }

            return this;
        }

        public T Get<T>(string key)
        {
            T result = default(T);

            if (data.ContainsKey(key))
            {
                result = (T)data[key];
            }

            return result;
        }
    }
    
}
