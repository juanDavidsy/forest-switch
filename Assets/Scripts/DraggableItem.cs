using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class DraggableItem : MonoBehaviour, IBeginDragHandler , IDragHandler , IEndDragHandler
{
    internal Transform parentAfterDrag;
    public Transform originalParent;
    public Slot slot;
    internal ItemProperties itemProper;
    public Image image;
    public Sprite spriteSlotVacio;
    bool firstTime;


    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!firstTime)
        {
            firstTime = true;
            originalParent = transform.parent;
        }
        parentAfterDrag = transform.parent;
        if (!slot.empty)
        {
            if (slot.numberOfObjects==1)
            {
                slot.TurnOffIcon(false);
            }
            transform.SetParent(transform.root);
            transform.SetAsLastSibling();
            image.sprite = slot.slotIconGameObject.GetComponent<Image>().sprite;
            itemProper = slot.slotProperties;
            parentAfterDrag.GetComponent<DropItem>().empty = true;
            image.raycastTarget = false;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!slot.empty)
        {
            transform.position = Input.mousePosition;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.SetParent(parentAfterDrag);
        transform.parent.GetComponent<DropItem>().empty = false;
        image.raycastTarget = true;
        if (originalParent ==  transform.parent)
        {
            slot.TurnOffIcon(true);
        }
    }

    public void BackToOriginalParent()
    {
        parentAfterDrag.GetComponent<DropItem>().empty = true;
        transform.parent = originalParent;
    }

    public void Resetear()
    {
        parentAfterDrag.GetComponent<DropItem>().empty = true;
        transform.parent = originalParent;
        image.sprite = spriteSlotVacio;
        slot.TurnOffIcon(true);
    }
}
