using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 //[CreateAssetMenu(fileName = "PlayerController", menuName = "InputController/PlayerController")]
 public class PlayerController : Input_Controller
    {
        public override bool RetrieveJumpInput()
        {
            return Input.GetButtonDown("Jump");
        }

        public override float RetrieveMoveInput()
        {
            return Input.GetAxisRaw("Horizontal");
        }

       public override bool RetrivejumpHoldInput()
       {
            return Input.GetButtonDown("Jump");
       }

}

