using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 public abstract class Input_Controller : MonoBehaviour
    {
        public abstract float RetrieveMoveInput();
        public abstract bool RetrieveJumpInput();
        public abstract bool RetrivejumpHoldInput();

    }

