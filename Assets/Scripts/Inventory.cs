using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Inventory : MonoBehaviour
{
    public bool inventoryEnabled;
    public GameObject inventory;
    public InventoryCraft inventoryCraft;

    private int[] allSlot;

    internal ItemProperties.Tipo[] tipoSlot;
    public Dictionary<ItemProperties.Tipo, GameObject[]> slots;
    
    public GameObject[] slotHolders;
    private const int maxNumberObj = 10;

    void Start()
    {
        slots = new Dictionary<ItemProperties.Tipo, GameObject[]>();
        allSlot = new int[slotHolders.Length];
        
        tipoSlot = (ItemProperties.Tipo[])System.Enum.GetValues(typeof(ItemProperties.Tipo));

        for (int i = 0; i < slotHolders.Length; i++)
        {
            allSlot[i] = slotHolders[i].transform.childCount;
            slots.Add(tipoSlot[i], new GameObject[allSlot[i]]);
        }

        for (int i = 0; i < allSlot.Length; i++)
        {
            for (int j = 0; j < allSlot[i]; j++)
            {
                slots[tipoSlot[i]][j] = slotHolders[i].transform.GetChild(j).gameObject;
                slots[tipoSlot[i]][j].GetComponent<Slot>().Comenzar(this);
            }
        }
        for (int i = 1; i < slotHolders.Length; i++)
        {
            slotHolders[i].SetActive(false);
        }
        inventoryCraft = GetComponent<InventoryCraft>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            inventoryEnabled = !inventoryEnabled;
        }

        if (inventoryEnabled)
        {
            inventory.SetActive(true);
            Cursor.visible = true;
            Time.timeScale = 0;
        }
        else
        {
            inventory.SetActive(false);
            Cursor.visible = false;
            inventoryCraft.VaciarMesaCrafteo();
            Time.timeScale = 1;
        }
    }

    public void AddItem(ItemProperties item)
    {
        for (int i = 0; i < slots[item.type].Length; i++)
        {
            Slot slotAdd = slots[item.type][i].GetComponent<Slot>();
            if (slotAdd.slotProperties.nombre == item.nombre && slotAdd.numberOfObjects < maxNumberObj)
            {
                slotAdd.numberOfObjects++;
                slotAdd.UpdateNumberObj();
                break;
            }
            else if (slotAdd.empty)
            {
                slotAdd.slotProperties = item;
                slotAdd.numberOfObjects++;
                slotAdd.UpdateSlot();
                slotAdd.UpdateNumberObj();
                slotAdd.empty = false;
                break;
            }
        }
    }

   
    public void RemoveItem(ItemProperties item, int cantidad = 0)
    {
        for (int j = 0; j < cantidad; j++)
        {
            for (int i = 0; i < slots[item.type].Length; i++)
            {
                Slot slotAdd = slots[item.type][i].GetComponent<Slot>();

                if (slotAdd.slotProperties.nombre == item.nombre)
                {
                    if (RemoveItemDirectly(slotAdd,cantidad))
                    {
                        break;
                    }
                }
            }
        }
    }

    public bool RemoveItemDirectly(Slot slot, int cantidad = 0)
    {
        for (int j = 0; j < cantidad; j++)
        {
            if (slot.numberOfObjects > 1)
            {
                slot.numberOfObjects--;
                slot.UpdateNumberObj();
                return true;
            }
            else
            {
                slot.slotProperties = slot.slotVacio;
                slot.empty = true;
                slot.numberOfObjects--;
                slot.UpdateSlot();
                slot.UpdateNumberObj();
                return true;
            }
            
        }
        return false;
    }


}
