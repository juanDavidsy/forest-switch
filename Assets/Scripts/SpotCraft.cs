using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotCraft : MonoBehaviour
{
    public GameObject craftwindow;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            craftwindow.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            craftwindow.SetActive(false);
        }
    }
}
