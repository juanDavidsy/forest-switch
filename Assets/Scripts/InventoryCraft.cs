using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryCraft : MonoBehaviour
{
    public Inventory inventory;
    public Transform dropParent;
    internal List<DropItem> dropItem = new List<DropItem>();
    public EscenciaBlanca cantidadEscenciaBlanca;
    public float essenceNecesaryAmount;
    void Start()
    {
        cantidadEscenciaBlanca = GetComponent<EscenciaBlanca>();
        inventory = GetComponent<Inventory>();
        for (int i = 0; i < dropParent.childCount; i++)
        {
            dropItem.Add(dropParent.GetChild(i).GetComponent<DropItem>());
        }
    }

    public void craftear(ItemProperties itemToCraft)
    {
        int itemCompleto = 0;
        foreach (var i in itemToCraft.materialsToCraft)
        {
            foreach (var j in dropItem)
            {
                if (j.itemProper!=null && i.material.nombre == j.itemProper.nombre)
                {
                    if (j.slotparent.numberOfObjects>=i.cantidad)
                    {
                        itemCompleto++;
                    }
                }
            }
        }

        if (itemCompleto==itemToCraft.materialsToCraft.Length && cantidadEscenciaBlanca.currentEssence>=essenceNecesaryAmount)
        {
            cantidadEscenciaBlanca.CambiarCantidadEscencia(-5);
            foreach (var item in dropItem)
            {
                if (item.slotparent!=null)
                {
                    inventory.RemoveItemDirectly(item.slotparent,1);//Falta la cantidad
                    switch (item.slotparent.numberOfObjects)
                    {
                        case 0:
                            item.transform.GetChild(0).GetComponent<DraggableItem>().Resetear();
                            break;
                        case 1:
                            item.slotparent.TurnOffIcon(false);
                            break;
                    }
                }
            }
            inventory.AddItem(itemToCraft);
        }
    }

    public void VaciarMesaCrafteo()
    {
        foreach (var item in dropItem)
        {
            if (!item.empty)
            {
                item.transform.GetChild(0).GetComponent<DraggableItem>().BackToOriginalParent();
            }
        }
    }
}
