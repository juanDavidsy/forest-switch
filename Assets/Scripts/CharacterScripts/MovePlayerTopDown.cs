using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayerTopDown : MonoBehaviour
{
    public Animator controller;
    private Rigidbody2D rb;
    [SerializeField] private float speed = 3f;
    private Vector2 moveinput;

    public Attack ataque;

    Vector2 lastMove;
    bool onAttack;
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        ataque = GetComponent<Attack>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale!=0)
        {
            float moveX = Input.GetAxisRaw("Horizontal");
            float moveY = Input.GetAxisRaw("Vertical");

            moveinput = new Vector2(moveX, moveY).normalized;
            controller.SetFloat("MovementY", moveinput.y);      
            controller.SetFloat("MovementX", moveinput.x);

            if (moveX != 0 )
            {
                GetComponent<SpriteRenderer>().flipX = !Mathf.Approximately(moveX, 1);
                lastMove = moveinput;
            }
            if (moveY != 0)
            {
                lastMove = moveinput;
            }
            if (Input.GetButtonDown("Fire1"))
            {
                onAttack = true;
                StartCoroutine(IWaitOnAttack());
                controller.SetTrigger("AttackOn");
                controller.SetFloat("AttackX", lastMove.x);
                controller.SetFloat("AttackY", lastMove.y);
                ataque.HacerAtaque(lastMove);
            }


        }  
    }

    private void FixedUpdate()
    {
        if (!onAttack)
        {
            rb.MovePosition(rb.position + moveinput * speed * Time.fixedDeltaTime);
        }

    }

    IEnumerator IWaitOnAttack()
    {
        yield return new WaitForSeconds(0.5f);
        onAttack = false;
    }
}
