using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
    public bool OnGround { get; private set; }
    public bool onWall { get; private set; }
    public float Friction { get; private set; }

    public Vector2 contactNormal { get; private set; }
    private PhysicsMaterial2D _material;
  
    private void OnCollisionExit2D(Collision2D collision)
    {
        OnGround = false;
        Friction = 0;
        onWall = false;
    }
  
    private void OnCollisionEnter2D(Collision2D collision)
    {
        EvaluateCollision(collision);
        RetrieveFriction(collision);
    }
  
    private void OnCollisionStay2D(Collision2D collision)
    {
        EvaluateCollision(collision);
        RetrieveFriction(collision);
    }


    public void EvaluateCollision(Collision2D collision)
    {
        for (int i = 0; i < collision.contactCount; i++)
        {
            contactNormal = collision.GetContact(i).normal;
            OnGround |= contactNormal.y >= 0.9f;
            onWall = Mathf.Abs(contactNormal.x) >= 0.9f;
        }
    }

    private void RetrieveFriction(Collision2D collision)
    {
        _material = collision.rigidbody.sharedMaterial;

        Friction = 0;

        if (_material != null)
        {
            Friction = _material.friction;
        }
    }

    public bool GetonGround()
    {
        return OnGround;
    }

    public float GetFriction()
    {
        return Friction;
    }


}
