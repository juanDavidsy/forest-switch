using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Jump : MonoBehaviour
{
    public Move move;
    [SerializeField] private Input_Controller input = null;
    [SerializeField, Range(0f, 10f)] private float jumpHeight = 3f;
    [SerializeField, Range(0, 5)] private float maxAirJumps = 0;
    [SerializeField, Range(0f, 5f)] private float downwardMovementMultiplier = 3f;
    [SerializeField, Range(0f, 5f)] private float upwardMovementMultiplier = 1.7f;
    [SerializeField, Range(0f, 0.3f)] private float coyoteTime = 0.2f;
    [SerializeField, Range(0f, 0.3f)] private float jumpBufferTime = 0.2f;

    private Rigidbody2D body;
    private Ground ground;
    private Vector2 velocity;

    private int jumPhase;
    private float defaultGravityScale;
    private float counter;
    private float jumpBufferCounter;

    private bool desiredJump;
    private bool onGround;
    private bool isJumping;

    private void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        ground = GetComponent<Ground>();

        defaultGravityScale = 1f;

    }

    // Start is called before the first frame update
    void Start()
    {
        move = GetComponent<Move>();
        
    }

    // Update is called once per frame
    void Update()
    {
        desiredJump |= input.RetrieveJumpInput();
    }

    private void FixedUpdate()
    {
        onGround = ground.GetonGround();
        velocity = body.velocity;

        if (onGround && body.velocity.y == 0)
        {
            jumPhase = 0;
            counter = coyoteTime;
            isJumping = false;
        }
        else
        {
            counter -= Time.deltaTime;
        }
       
        if (desiredJump)
        {
            desiredJump = false;
            jumpBufferCounter = jumpBufferTime;
          
        }
        else if(!desiredJump && jumpBufferCounter > 0)
        {
            jumpBufferCounter -= Time.deltaTime;
        }
       
        if(jumpBufferCounter > 0)
        {
            move.animator.SetTrigger("Jump");
            JumpAction();
        }  
       
        if( input.RetrivejumpHoldInput() && body.velocity.y > 0)
        {
            body.gravityScale = upwardMovementMultiplier;
        }
        else if(!input.RetrivejumpHoldInput() || body.velocity.y < 0)
        {
            body.gravityScale = downwardMovementMultiplier;
        }
        else if(body.velocity.y == 0)
        {
            body.gravityScale = defaultGravityScale;
        }

        body.velocity = velocity;
    }
    private void JumpAction()
    {
        if(counter > 0f || jumPhase < maxAirJumps && isJumping)
        {
            if (isJumping)
            {
              jumPhase += 1;
            }
            jumpBufferCounter = 0;
            counter = 0;
            float jumpSpeed = Mathf.Sqrt(-2f * Physics2D.gravity.y * jumpHeight);
            isJumping = true;
           
            if(velocity.y > 0f)
            {
                jumpSpeed = Mathf.Max(jumpSpeed - velocity.y, 0f);
                
            }
            velocity.y += jumpSpeed;
        }
    }


}
