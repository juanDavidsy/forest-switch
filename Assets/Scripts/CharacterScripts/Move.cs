using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Move : MonoBehaviour
{
    public Animator animator;
    
    [SerializeField] private Input_Controller input = null;
    [SerializeField, Range(0f, 100f)] private float maxSpeed = 4f;
    [SerializeField, Range(0f, 100f)] private float maxAcceleration = 35f;
    [SerializeField, Range(0f, 100f)] private float maxAirAcceleration = 20f;
    [SerializeField, Range(0.05f, 0.5f)] private float wallStickTime = 0.25f;
    [SerializeField] private float timeAttack;
    private bool attackReady = true;

    private Vector2 direction, desiredVelocity, velocity;
    private Rigidbody2D body;
    private Ground ground;
    private Wall_Interaction wall_Interaction;

    private float maxSpeedChange, acceleration , wallStickCounter;
    private bool onGround;

    public Attack ataque;

    float lastMoveX;
    private void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        ground = GetComponent<Ground>();
        wall_Interaction = GetComponent<Wall_Interaction>();
        ataque = GetComponent<Attack>();
        
    }

    private void Update()
    {
        if (Time.timeScale!=0)
        {
            direction.x = input.RetrieveMoveInput();
            desiredVelocity = new Vector2(direction.x, 0f) * Mathf.Max(maxSpeed - ground.Friction, 0f);
            animator.SetFloat("Speed", Mathf.Abs(direction.x));
            //print(direction.x);
            if (Mathf.Abs(direction.x)>0)
            {
                GetComponent<SpriteRenderer>().flipX = !Mathf.Approximately(direction.x,1);
            }
            if (direction.x!=0)
            {
                lastMoveX = direction.x;
            }
            if (Input.GetButtonDown("Fire1"))
            {
                if (attackReady)
                {
                    attackReady = false;
                    animator.SetTrigger("Atack");
                    ataque.HacerAtaque(new Vector2(lastMoveX,0));
                    StartCoroutine(TiempoEsperaAtaque());
                }
            }

        }

    }
    IEnumerator TiempoEsperaAtaque()
    {
        yield return new WaitForSeconds(timeAttack);
        attackReady = true;
    }

    private void FixedUpdate()
    {
        onGround = ground.OnGround;
        velocity = body.velocity;

        acceleration = onGround ? maxAcceleration : maxAirAcceleration;
        maxSpeedChange = acceleration * Time.deltaTime;
        velocity.x = Mathf.MoveTowards(velocity.x, desiredVelocity.x, maxSpeedChange);

        if(ground.onWall && !ground.OnGround && !wall_Interaction.wallJumping)
        {
            if(wallStickCounter > 0)
            {
                velocity.x = 0;

                if(input.RetrieveMoveInput() == ground.contactNormal.x)
                {
                    wallStickCounter -= Time.deltaTime;
                }
                else
                {
                    wallStickCounter = wallStickTime;
                }
            
            }
        }
       
        body.velocity = velocity;
    }
}



