using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall_Interaction : MonoBehaviour
{
    [SerializeField] private Input_Controller input = null;
    [SerializeField] [Range(0.1f, 5f)] private float wallSlideSpeed = 2f;
    [SerializeField] private Vector2 wallJumpClimb = new Vector2(4f, 12);
    [SerializeField] private Vector2 wallJumpBounce = new Vector2(10.7f, 10f);
    [SerializeField] private Vector2 wallJumpLeap = new Vector2(14f, 12f);

    private Ground ground;
    private Rigidbody2D body;

    private Vector2 velocity;
    public bool onwall;
    public bool onGround;
    public bool desiredJump;
    public bool wallJumping;
    private float wallDirectionx;
      
    // Start is called before the first frame update
    void Start()
    {
        ground = GetComponent<Ground>();
        body = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if(onwall && !onGround)
        {
            desiredJump = input.RetrieveJumpInput();
        }
    }

    private void FixedUpdate()
    {
        velocity = body.velocity;
        onwall = ground.onWall;
        onGround = ground.OnGround;
        wallDirectionx = ground.contactNormal.x;

        if (onwall)
        {
            if(velocity.y <-wallSlideSpeed)
            {
                velocity.y = -wallSlideSpeed;
            }
        }

        if((onwall && velocity.x == 0) || onGround)
        {
            wallJumping = false;
        }


        if (desiredJump)
        {
            if(wallDirectionx == input.RetrieveMoveInput())
            {
                velocity = new Vector2(wallJumpClimb.x * wallDirectionx, wallJumpClimb.y);
                wallJumping = true;
                desiredJump = false;
            }
            else if(input.RetrieveMoveInput() == 0)
            {
                velocity = new Vector2(wallJumpBounce.x * wallDirectionx, wallJumpBounce.y);
                wallJumping = true;
                desiredJump = false;
            }
            else
            {
                velocity = new Vector2(wallJumpLeap.x * wallDirectionx, wallJumpLeap.y);
                wallJumping = true;
                desiredJump = false;

            }
        }
        
        body.velocity = velocity;
    
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        ground.EvaluateCollision(collision);

        if(ground.onWall && ! ground.OnGround && wallJumping)
        {
            body.velocity = Vector2.zero;
        }
    }


}
