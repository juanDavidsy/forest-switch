using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    public Transform centroGolpe;
    public float radioGolpe;
    float longitudCentroGolpe;
    Vector3 cGolpe = new Vector3();
    // Start is called before the first frame update

    public float damage;
    void Start()
    {
        longitudCentroGolpe = Vector3.Distance(transform.position, centroGolpe.position);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HacerAtaque(Vector2 posicion)
    {
        bool missed = true;
        cGolpe = transform.position + new Vector3(posicion.x,posicion.y,0)*longitudCentroGolpe;
        Collider2D[] objetosAtacados = Physics2D.OverlapCircleAll(cGolpe,radioGolpe);
        //print($"adventurepsoition{transform.position} ,  vector2{posicion}, longitudcentrogolpe{longitudCentroGolpe},   cGolpe{cGolpe}");
        transform.GetChild(0).position = cGolpe;
        foreach (var item in objetosAtacados)
        {
            if (item.CompareTag("Enemy"))
            {
               
                item.GetComponent<Life_bar>().TakeDamage(damage);
                missed = false;
                Sounds.sound.Active_SoundsFx(transform.position, 0);
            }
        }
        if (missed)
        {
            Sounds.sound.Active_SoundsFx(transform.position, 1);
        }

    }
    public void EnemyAttack()
    {
        Collider2D[] objetosAtacados = Physics2D.OverlapCircleAll(centroGolpe.position,radioGolpe);
        foreach (var item in objetosAtacados)
        {
            if (item.CompareTag("Player"))
            {
               
                item.GetComponent<Life_bar>().TakeDamage(damage);
            }
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(centroGolpe.position, radioGolpe);
    }
}
