using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaDeteccion : MonoBehaviour
{
    public bool jugadorEnArea;
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            jugadorEnArea = true;

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            jugadorEnArea = false;
    }
}
