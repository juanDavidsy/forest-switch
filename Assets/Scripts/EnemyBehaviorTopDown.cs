using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class EnemyBehaviorTopDown : MonoBehaviour
{
    public Transform jugador;
    public MoveEnemyTopDown movimientoTopDown;
    public Animator animator;
    public Attack attack;

    public float distanciaAtaque, duracionTiempoAtaque, duracionTiempoPatrulla;
    internal float tiempoAtaque,tiempoPatrulla;


    public AreaDeteccion fueraDeRango, areaDeAtaque,areaEnemigoVisible;

    public List<Transform> puntosPatrulla = new List<Transform>();
    public bool patrullar, enemigoNeutro;

    Vector3 siguientePuntoPatrulla;
    int siguientePuntoPatrullaje;


    private void Start()
    {
        movimientoTopDown = GetComponent<MoveEnemyTopDown>();
    }
    private void OnEnable()
    {
        tiempoPatrulla = 0;
    }
    void Update()
    {
        if (!enemigoNeutro)
        {
            if (fueraDeRango.jugadorEnArea || areaDeAtaque.jugadorEnArea)
            {
                if (fueraDeRango.jugadorEnArea == true && areaDeAtaque.jugadorEnArea == false)
                {
                    if (tiempoAtaque != 0)
                    {
                        tiempoAtaque += Time.deltaTime;
                    }
                    if (tiempoAtaque >= duracionTiempoAtaque)
                    {
                        tiempoAtaque = 0;
                    }
                    movimientoTopDown.Mover(jugador.transform.position);
                    movimientoTopDown.onMove = true;
                }
                if (areaDeAtaque.jugadorEnArea == true)
                {
                    movimientoTopDown.onMove = false;
                    if (tiempoAtaque >= duracionTiempoAtaque || tiempoAtaque == 0)
                    {
                        animator.SetTrigger("Attack");
                        tiempoAtaque = 0;

                    }
                    tiempoAtaque += Time.deltaTime;

                }
            }
            else
            {
                HacerPatrulla();
            }
        }
        else
        {
            HacerPatrulla();
        }
    }

    void HacerPatrulla()
    {
        if (areaEnemigoVisible.jugadorEnArea == true && patrullar == true)
        {
            if (!movimientoTopDown.onMove)
            {
                if (tiempoPatrulla == 0)
                {
                    animator.SetTrigger("Idle");
                }
                tiempoPatrulla += Time.deltaTime;
                if (tiempoPatrulla >= duracionTiempoPatrulla)
                {
                    tiempoPatrulla = 0;
                    movimientoTopDown.Mover(SiguientePuntoPatrulla());
                }
            }
        }
    }
    public Vector3 SiguientePuntoPatrulla()
    {
        siguientePuntoPatrullaje = Random.Range(0, puntosPatrulla.Count);
        while (siguientePuntoPatrulla == puntosPatrulla[siguientePuntoPatrullaje].position)
        {
            siguientePuntoPatrullaje = Random.Range(0, puntosPatrulla.Count);
        }
        siguientePuntoPatrulla = puntosPatrulla[siguientePuntoPatrullaje].position;
        return siguientePuntoPatrulla;
    }
}
