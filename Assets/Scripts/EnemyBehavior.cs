using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyBehavior : Input_Controller
{
    public Transform jugador;


    public float distanciaAtaque, duracionTiempoAtaque, duracionTiempoPatrulla;//distanciaDetenccion
    float tiempoAtaque,tiempoPatrulla;


    AreaDeteccion areaDeteccionJugadorUno, areaDeteccionJugadorDos,areaEnemigoVisible;
    Vector3 siguientePuntoPatrulla;
    public List<Vector3> puntosPatrulla = new List<Vector3>();
    int siguientePuntoPatrullaje;
    public bool patrullar;


    //bool escoger = false;

    private void Start()
    {
        areaDeteccionJugadorUno = transform.parent.Find("1AreaDeteccionJugador").GetComponent<AreaDeteccion>();
        areaDeteccionJugadorDos = transform.Find("2AreaDeteccionJugador").GetComponent<AreaDeteccion>();
        areaEnemigoVisible = transform.Find("AreaEnemigoVisible").GetComponent<AreaDeteccion>();
        for (int i = 0; i < transform.parent.Find("PuntosPatrulla").childCount; i++)
        {
            puntosPatrulla.Add(transform.parent.Find("PuntosPatrulla").GetChild(i).GetComponent<Transform>().position);
        }
        if (patrullar==true)
        {
            areaDeteccionJugadorUno.gameObject.SetActive(false);
        }
        SiguientePuntoPatrulla();

    }

   
    
    public override float RetrieveMoveInput()
    {
        
        
        if (areaDeteccionJugadorDos.jugadorEnArea == true || areaDeteccionJugadorUno.jugadorEnArea == true)
        {
            if (Vector3.Distance(jugador.position, transform.position) <= distanciaAtaque)
            {
  
                if (tiempoAtaque >= duracionTiempoAtaque || tiempoAtaque==0)
                {
                    //atacar
                    tiempoAtaque = 0;
         
                }
                tiempoAtaque += Time.deltaTime;
                
            }
            else //if (Vector3.Distance(jugador.position,transform.position) <= distanciaDetenccion)
            {
                if (tiempoAtaque!=0)
                {
                    tiempoAtaque += Time.deltaTime;
                }
                if (tiempoAtaque >= duracionTiempoAtaque)
                {
                    tiempoAtaque = 0;
                }
                return MoverHacia(jugador.position);
            }
        }
        else if(areaEnemigoVisible.jugadorEnArea==true && patrullar==true)
        {
            if (tiempoPatrulla==0)
            {
                if (Vector3.Distance(siguientePuntoPatrulla, transform.position)>=0.9f)
                {
                    return MoverHacia(siguientePuntoPatrulla);
                }
                else
                {
                    SiguientePuntoPatrulla();
                    tiempoPatrulla += Time.deltaTime;
                }
            }
            else
            {
                print("aaaaaaaaaaaaaaaaaaaaaa");
                tiempoPatrulla += Time.deltaTime;
                if (tiempoPatrulla>=duracionTiempoPatrulla)
                {
                    tiempoPatrulla = 0;
                }
                print(tiempoPatrulla);
               
            }
            
        }
        return 0;
        //Ense�ar al enemigo a volver al punto de inicio
    }

    public void SiguientePuntoPatrulla()
    {
        siguientePuntoPatrullaje = Random.Range(0, puntosPatrulla.Count);
        while (Vector3.Distance(puntosPatrulla[siguientePuntoPatrullaje], transform.position) < 0.3f)
        {
            siguientePuntoPatrullaje = Random.Range(0, puntosPatrulla.Count);
        }
        siguientePuntoPatrulla = puntosPatrulla[siguientePuntoPatrullaje];
    }
    public float MoverHacia(Vector3 objetivo)
    {
        if (objetivo.x-transform.position.x>=0)
            return 1f;
        else
            return -1f;
    }

    public override bool RetrieveJumpInput()
    {
        throw new System.NotImplementedException();
    }

    public override bool RetrivejumpHoldInput()
    {
        throw new System.NotImplementedException();
    }
}
