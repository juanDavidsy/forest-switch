using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class FromDestination : MonoBehaviour
{
   
    public DatosPortal datosPortal;
    private void OnEnable()
    {
        SceneManager.sceneLoaded += Verificar;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= Verificar;
    }
    public void Verificar(Scene scene, LoadSceneMode mode)
    {
        //if (SceneManager.GetActiveScene().name != datosPortal.nombreMapaActual)
        //{
        if (datosPortal.onTp)
        {
            BuscarObjecto();
            datosPortal.onTp = false;
        }
        //}
    }

    public void BuscarObjecto()
    {
        PasarPosicion(GameObject.FindGameObjectWithTag("Portales").transform.Find(datosPortal.nombrePortal));
    }
    public void PasarPosicion(Transform objecto)
    {
       GameObject.FindGameObjectWithTag("Player").GetComponent<ToDestination>().Transportar(objecto.position);  
    }


}
