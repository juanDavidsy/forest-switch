using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidosAmbiente : MonoBehaviour
{
    public AudioSource enviroment;
    public AudioClip[] sonidos;

   
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && this.tag == "Bosque")
        {
            enviroment.clip = sonidos[0];
            enviroment.Play();
        }
   
        if (collision.CompareTag("Player") && this.tag == "Cueva")
        {
            enviroment.clip = sonidos[1];
            enviroment.Play();
        }

        if (collision.CompareTag("Player") && this.tag == "Tumbas")
        {
            enviroment.clip = sonidos[2];
            enviroment.Play();
        }

    }


    


}
