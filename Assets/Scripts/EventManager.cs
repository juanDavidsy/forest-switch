using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace FSEventos
{
    
    public class DataEvent : UnityEvent<EventData> { }
    public class EventManager : MonoBehaviour
    {
        private Dictionary<EventNames, DataEvent> events;

        private static EventManager eventManager;
        private static EventManager instance
        {
            get
            {
                if (!eventManager)
                {
                    eventManager = FindObjectOfType<EventManager>();
                    if (!eventManager)
                    {
                        print("No hay eventmanager");

                    }
                    else
                    {
                        eventManager.Init();
                    }
                }
                return eventManager;
            }

        }
        private void Init()
        {
            if (events == null)
            {
                events = new Dictionary<EventNames, DataEvent>();
            }
        }

        public static void StartListening(EventNames eventName, UnityAction<EventData> listener)
        {
            DataEvent e = null;
            if (instance.events.TryGetValue(eventName, out e))
            {
                e.AddListener(listener);
            }
            else
            {
                e = new DataEvent();
                e.AddListener(listener);

                instance.events.Add(eventName, e);
            }
        }
        public static void StopListening(EventNames eventName, UnityAction<EventData> listener)
        {
            DataEvent e = null;
            if (eventManager == null)
                return;
            if (instance.events.TryGetValue(eventName, out e))
            {
                e.RemoveListener(listener);
            }
        }

        public static void TriggerEvent(EventData data)
        {
            DataEvent e = null;
            if (instance.events.TryGetValue(data.name, out e))
            {
                e.Invoke(data);
            }
            
            
        }
        private void OnDestroy()
        {
            if (eventManager == null)
                return;

            foreach (var e in instance.events.Values)
            {
                e.RemoveAllListeners();
            }
        }

    }
}

