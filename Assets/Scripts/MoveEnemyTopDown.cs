using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveEnemyTopDown : MonoBehaviour
{
    public float speed;

    Vector3 posicionFinal = new Vector3();

    internal bool onMove;

    public Animator animator;

    public bool movimientoTopDown;
    void Start()
    {
        animator = GetComponent<Animator>();
        posicionFinal = transform.position;
    }


    void Update()
    {
        if (onMove)
        {
            if (!movimientoTopDown)
            {
                posicionFinal = new Vector3(posicionFinal.x,transform.position.y,0);
            }
            if (transform.position != posicionFinal)
            {
                transform.position = Vector2.MoveTowards(transform.position, posicionFinal, speed * Time.deltaTime);
            }
            else
            {
                onMove = false;
                //animator.SetInteger("Idle", 0);
            }
        }
    }
    public void Mover(Vector3 posicionDestino)
    {
        posicionFinal = posicionDestino;
        if (movimientoTopDown)
        {
            if (Mathf.Abs(transform.position.x-posicionFinal.x)>= Mathf.Abs(transform.position.y - posicionFinal.y))
            {
                GetComponent<SpriteRenderer>().flipX = Mathf.Approximately(Mathf.Sign(transform.position.x - posicionFinal.x), 1);
                animator.SetFloat("MovementX", Mathf.Sign(transform.position.x - posicionFinal.x) * -1);
            }
            else
            {
                animator.SetFloat("MovementX", 0);
                animator.SetFloat("MovementY", Mathf.Sign(transform.position.y - posicionFinal.y)*-1);
            }

        }
        else
        {
            GetComponent<SpriteRenderer>().flipX = Mathf.Approximately(Mathf.Sign(transform.position.x - posicionFinal.x), 1);
            animator.SetFloat("MovementX",1);
        }

        onMove = true;
        animator.SetTrigger("OnMovement");
    }


}
