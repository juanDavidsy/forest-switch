using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Datos", menuName = "DatosPortal/datos")]
public class DatosPortal : ScriptableObject
{
    public string nombreMapaActual;
    public string nombrePortal;
    public bool onTp;
}
